import { api } from '../utils/api'
import { store } from './store'
export const getStates = async () => {
    store.dispatch({ type: 'GET_STATES' })
    let response = await api.get('provinces')
    if (response && response.status === 200) {
        store.dispatch({ type: 'GET_STATES_DONE', data: response.data.data })
    } else {
        return
    }
}
export const getCities = async (id) => {
    store.dispatch({ type: 'GET_CITIES' })
    let response = await api.get(`provinces/${id}/cities`)
    if (response && response.status === 200) {
        store.dispatch({ type: 'GET_CITIES_DONE', data: response.data.data })
    } else {
        return
    }
}
export const getRegions = async (id, parentId) => {
    store.dispatch({ type: 'GET_REGIONS' })
    let response = await api.get(`provinces/${parentId}/cities/${id}/districts`)
    if (response && response.status === 200) {
        store.dispatch({ type: 'GET_REGIONS_DONE', data: response.data.data })
    } else {
        return
    }
}

export const clearStore=()=>{
    store.dispatch({type:'CLEAR_STORE'})
}