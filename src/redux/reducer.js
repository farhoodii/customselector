
const initialState = {
    states: undefined,
    statesLoading: false,
    cities: undefined,
    citiesLoading: false,
    regions: undefined,
    regionsLoading: false
}


const reducer = (state = initialState, action) => {
    switch (action.type) {
        case 'GET_STATES':
            return {
                ...state,
                statesLoading: true
            }
        case 'GET_STATES_DONE':
            return {
                ...state,
                statesLoading: false,
                states: action.data
            }
        case 'GET_CITIES':
            return {
                ...state,
                citiesLoading: true
            }
        case 'GET_CITIES_DONE':
            return {
                ...state,
                citiesLoading: false,
                cities: action.data
            }
        case 'GET_REGIONS':
            return {
                ...state,
                regionsLoading: true
            }
        case 'GET_REGIONS_DONE':
            return {
                ...state,
                regionsLoading: false,
                regions: action.data
            }
        case 'CLEAR_STORE':
            return {
                ...state,
                states: undefined,
                statesLoading: false,
                cities: undefined,
                citiesLoading: false,
                regions: undefined,
                regionsLoading: false
            }
        default: return state
    }
}


export default reducer