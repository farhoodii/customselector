import styled from 'styled-components'
const Style = styled.div`
.container{
    width:100%;
    min-height:100vh;
    background:white;
    display:flex;
    justify-content:space-around;
    flex-direction:row;
    flex-wrap:wrap;
    align-items:center;
    background:#00bfff;
}
.container > *{
    margin-top:50px;
}
`
export default Style