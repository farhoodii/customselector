import React from 'react'
import Style from './style'
import CustomSelector from '../../components/customSelector'
class Main extends React.Component {
    handlesubmit = (e) => {
        window.alert(JSON.stringify(e))
    }
    render() {
        return (
            <Style>
                <div className="container">
                    <CustomSelector  onsubmit={this.handlesubmit} validation={['city', 'region']} />
                </div>

            </Style>
        )

    }
}
export default Main