import { create } from "apisauce";


export const api = create({
    baseURL: 'https://api.tavanito.com/v1/',
    headers: {
        "Accept-Language": "fa"
    }
});
