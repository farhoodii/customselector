import styled from 'styled-components'
const Style = styled.div`
.main{
    background:#222121;
    min-height:470px;
    border-radius:12px;
    display:flex;
    align-items:center;
    flex-direction:column;
    .selectBox{
        padding:20px;
        margin-bottom:50px;
        height:50px;
        .select{
            min-width:300px;
        }
        h6{font-size:18px;color:white;}
    }
    .submitBox{
        padding:20px;
        height:50px;
        margin-top:50px;
        button{
            width:300px;
            height:40px;
            font-size:16px;
            font-weight:bold;
        }
        .errorBox{
            width:100%;
            display:flex;
            min-height:50px;
            flex-direction:row;
            justify-content:space-between;
            .errorTitle{
                font-size:18px;align-items:center;color:white;
            }
            .errorTexts{
                display:flex;
                flex-direction:row;
                justify-content:space-around;
                p{
                    margin: 0px;
                    text-align: center;
                    margin-left: 10px;
                    color: #ff0404;
                    font-weight:bold;
                }
            }
        }
     
    }
  
  
}
`
export default Style