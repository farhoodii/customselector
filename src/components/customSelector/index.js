import React from 'react'
import Style from './style'
import { Select, Spin, Button } from 'antd'
import { getStates, getCities, getRegions, clearStore } from '../../redux/actions'
import { connect } from "react-redux";
const { Option } = Select;

class CustomSelector extends React.Component {
    state = { states: [], cities: [], regions: [], selectedState: '', selectedCity: '', selectedRegion: '', validationError: '' }

    componentDidMount() {
        getStates()
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.states && nextProps.states.length > 0) {
            this.setState({
                states: nextProps.states
            })
        }
        if (nextProps.cities && nextProps.cities.length > 0) {
            this.setState({
                cities: nextProps.cities
            })
        }
        if (nextProps.regions && nextProps.regions.length > 0) {
            this.setState({
                regions: nextProps.regions
            })
        }
    }

    handleSelects = (e, key, data) => {
        const { id } = data.props.id
        switch (key) {
            case "states":
                getCities(id)
                this.setState({ selectedState: e, selectedCity: '', selectedRegion: '' })
                break;
            case "cities":
                const { parent_id } = data.props.id
                getRegions(id, parent_id)
                this.setState({ selectedCity: e, selectedRegion: '' })
                break;
            case "regions":
                this.setState({ selectedRegion: e })
                break;
            default: return
        }
    }

    handleValidation = () => {
        const states = { state: 'selectedState', city: 'selectedCity', region: 'selectedRegion' }
        const statesPersianNames = { state: 'استان', city: 'شهر', region: 'منطقه' }
        if (this.props.validation && this.props.validation.length > 0) {
            let requireSelects = []
            requireSelects = this.props.validation.filter((e => !this.state[states[e]] || this.state[states[e]] === ''))
            if (requireSelects && requireSelects.length > 0) {
                let errorArray = requireSelects.map((e) => {
                    return statesPersianNames[e]
                })
                return errorArray
            }
        } else { return false }
    }

    handleSubmit = () => {
        let data = { state: this.state.selectedState, city: this.state.selectedCity, region: this.state.selectedRegion }
        for (var i in data) {
            if (data[i] === "") {
                delete data[i]
            }
        }
        if (this.props.onsubmit) {
            this.props.onsubmit(data)
        }
    }

    componentWillUnmount() {
        clearStore()
    }

    render() {
        return (
            <Style>
                <div className='main'>
                    <div className='selectBox'>
                        {this.props.statesLoading ?
                            <Spin className='spin' />
                            :
                            <>
                                <h6>استان</h6>
                                <Select
                                    value={this.state.selectedState}
                                    onSelect={(e, data) => { this.handleSelects(e, 'states', data) }}
                                    showSearch
                                    className='select'
                                    size='large'
                                >
                                    {
                                        this.state.states.map((e) => <Option key={e.id} id={e} value={e.title}>{e.title}</Option>)
                                    }
                                </Select>
                            </>
                        }
                    </div>

                    <div className='selectBox'>
                        {
                            this.state.selectedState ?
                                !this.props.citiesLoading ?
                                    <>
                                        <h6>شهر</h6>
                                        <Select
                                            value={this.state.selectedCity}
                                            onSelect={(e, data) => { this.handleSelects(e, 'cities', data) }}
                                            showSearch
                                            className='select'
                                            size='large'
                                        >
                                            {
                                                this.state.cities.map((e) => <Option key={e.id} id={e} value={e.title}>{e.title}</Option>)
                                            }
                                        </Select>
                                    </>
                                    : <Spin />
                                : null
                        }
                    </div>

                    <div className='selectBox'>
                        {
                            this.state.selectedCity ?
                                !this.props.regionsLoading ?
                                    <>
                                        <h6>منطقه</h6>
                                        <Select
                                            value={this.state.selectedRegion}
                                            onSelect={(e, data) => { this.handleSelects(e, 'regions', data) }}
                                            showSearch
                                            className='select'
                                            size='large'
                                        >
                                            {
                                                this.state.regions.map((e) => <Option key={e.id} id={e} value={e.title}>{e.title}</Option>)
                                            }
                                        </Select>
                                    </>
                                    : <Spin />
                                : null
                        }
                    </div>

                    <div className='submitBox'>
                        <div className='errorBox'>
                            {
                                this.handleValidation() ?
                                    <>
                                        <p className='errorTitle'> فیلد های اجباری </p>
                                        <div className='errorTexts'>
                                            {this.handleValidation().map((e) => (
                                                <p key={e}>
                                                    {e}
                                                </p>
                                            )
                                            )
                                            }
                                        </div>
                                    </>
                                    : null
                            }
                        </div>
                        <Button onClick={this.handleSubmit} disabled={this.handleValidation()}>
                            ثبت اطلاعات
                        </Button>
                    </div>


                </div>
            </Style>
        )
    }
}
const mapStateToProps = (state) => {
    return ({
        states: state.states,
        statesLoading: state.statesLoading,
        cities: state.cities,
        citiesLoading: state.citiesLoading,
        regions: state.regions,
        regionsLoading: state.regionsLoading
    })
}
export default connect(mapStateToProps, null)(CustomSelector) 